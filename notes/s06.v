From mathcomp Require Import all_ssreflect.

Lemma iff_or n m :n * m = 0 <-> n = 0 \/ m = 0.
Proof.
split.
  move=> /eqP; rewrite muln_eq0 => /orP.
  move=> eqH; elim: eqH.
  Undo.
  elim.
  Undo.
  elim => /eqP.
    by move=> eqn0; left.
    Undo.
    by left.
  by move=> eqm0; right.
  Undo.
  by right.
move=> orH; elim: orH.
Undo.
elim.
Undo.
elim=> [->|->].
  by rewrite mul0n.
by rewrite muln0.
Qed.

Lemma existsH1 n : (exists m, m < n) -> n != 0.
Proof.
move=> exH; elim: exH.
Undo.
elim=> m mltnH.
case neq0: (n == 0).
  by move: mltnH; rewrite (eqP neq0) ltn0.
by [].
Abort.

Lemma existsH1 n : (exists m, m < n) -> n != 0.
Proof.
move=> exH; elim: exH.
Undo.
elim=> m mltnH.
case neq0: (n == 0) => //=.
by move: mltnH; rewrite (eqP neq0) ltn0.
Qed.

Lemma forallH (s : seq nat) : (forall s0 : seq nat, size s0 >= 0) -> size s >= 0.
Proof.
by move=> allsizeH; apply: (allsizeH s).
Undo.
by move=> /(_ s).
Abort.

Lemma forallH (s : seq nat) (allseqH : forall s0 : seq nat, size s0 >= 0) : size s >= 0.
Proof. by apply: (allseqH s). Qed.

(* The Record command describes tuples whose components, called fields,
   can be accessed with projections. Records can also be used to describe
    mathematical structures *)

Record poset := POSet{
  D : Set;
  R : D -> D -> Prop;
  refH : forall x, R x x;
  antisym : forall x y, R x y -> R y x -> x = y;
  transH : forall y x z, R x y -> R y z -> R x z }.

Lemma anti_leq n m : n <= m -> m <= n -> n = m.
Proof. by move=> leqnm leqmn; apply: anti_leq; rewrite leqnm leqmn. Qed.

Definition nat_poset := POSet nat leq leqnn anti_leq leq_trans.

(* Use Record to formalize groups i. e.
   G = < D, ., e >
   where D is a set, . is a binary operation on D and e is an element of D
   such that:
   a) Assoc: for all a b c in D, (a . b) . c = a . (b . c);
   b) Ident: for all a in D, a . e = a and e . a = a;
   c) Inverse: for all a in D, there is a *unique* element b in D such that
      (a . b) = e and (b . a) = e.
 *)

Require Import ZArith.

Print Z.

Check 0.
Compute 0 - 1.

Open Scope Z_scope.

Check 0.
Compute 0 - 1.

Close Scope Z_scope.

Check 0.
Compute 0 - 1.












Record group := Group{
  G : Set;
  op : G -> G -> G;
  i : G -> G;
  e : G;
  assoc : forall x y z, op (op x y) z = op x (op y z);
  ident : forall x, op e x = x /\ op x e = x;
  inv : forall x, op (i x) x = e /\ op x (i x) = e}.

Open Scope Z_scope.

Lemma Z_ident z : 0 + z = z /\ z + 0 = z.
Proof. by rewrite Z.add_0_r Z.add_0_l. Qed.

Definition Z_i z := - z.

Lemma Z_inv z : (Z_i z) + z = 0 /\ z + (Z_i z) = 0.
Proof. by rewrite Z.add_opp_diag_r Z.add_opp_diag_l. Qed.

Definition Z_group := Group Z Z.add Z_i 0 Zplus_assoc_reverse Z_ident Z_inv.


(* Z.add_assoc *)
