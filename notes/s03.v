(* SESSION #3: PROOFS BY COMPUTATION && CASE ANALYSIS *)

From mathcomp Require Import all_ssreflect.

(* Today we are going to state and prove some easy theorems *)

(* The commands that we use in the proof environment are called tactics

  Lemma id : A.
  Proof.
  tactic1
  tactic2
  ...
  Qed. *)

(* RECALL: We work on top of the Mathematical Components library and we follow
   the Small Scale Reflection approach using the SSReflect proof language. *)

(* RECALL_2 : if A : Prop  and  h : A  then "h is a proof of A" *)


(* Proof by computation *)

(* The by [] tactic solves trivial goal (mostly) by computation. *)

Lemma addn_2_2 : 2 + 2 = 4.
Proof.
by [].
Qed.

Lemma addnsuccn : forall n m : nat, n.+1 + m = (n + m).+1.
Proof. Abort.

Lemma succn_addn n m : n.+1 + m = (n + m).+1.
Proof.
by [].
Qed.

Lemma addn_succn n m : n + m.+1 = (n + m).+1.
Proof.
(* by [] => Fail *)
Abort.

Lemma mul0n n : 0 * n = 0.
Proof.
by [].
Qed.

Lemma leq_refl n : n <= n.
Proof.
by [].
Qed.

Lemma leq_refl_2 : forall n : nat, n <= n.
Proof.
Abort.

Lemma falseb b : false -> b.
Proof.
by [].
Qed.

(* We can perform proofs by case analysis. We use the tactic case: term
   to perform it. *)

Lemma negbK b : ~~ (~~ b) = b.
Proof.
(*  by [] => Fail *)
case: b.
  by [].
by [].
Qed.

Lemma negbK_tac b :  ~~ (~~ b) = b.
Proof.
case: b; by [].
Qed.

Lemma negbK_tac_2 b :  ~~ (~~ b) = b.
Proof.
by case: b.
Qed.

(* ---------------------------- EXERCISES ------------------------------------ *)
Lemma orb_classic b : b || ~~ b.
Proof.
Admitted.

Lemma andbC b1 b2 : b1 && b2 = b2 && b1.
Proof.
Admitted.

(* --------------------------------------------------------------------------- *)

Lemma negb_orbD b1 b2 : ~~ (b1 || b2) = ~~ b1 && ~~ b2.
Proof.
by case: b1; case: b2.
Qed.

Lemma negb_orbD_2 : forall b1 b2, ~~ (b1 || b2) = ~~ b1 && ~~ b2.
Proof.
move=> b1 b2.
by case: b1; case: b2.
Qed.

Lemma negb_orbD_nomove : forall b1 b2, ~~ (b1 || b2) = ~~ b1 && ~~ b2.
Proof.
(* by case; case. *)
Admitted.

Lemma leqn0 n : (n <= 0) = (n == 0).
Proof.
case: n.
  by [].
move=> n; by [].
(* by move => n *)
Qed.

Lemma leqn0_2 n : (n <= 0) = (n == 0).
Proof.
(* by case: n => [ | n]. *)
Admitted.

