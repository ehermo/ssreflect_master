(* SESSION #5: APPLY; MOVE: && REFLECT *)

From mathcomp Require Import all_ssreflect.

(* From previous session: *)

Lemma moveandrewrite b : b = true -> ~~ b = false.
Proof.
by move=> bistrue; rewrite bistrue.
Undo.
(* We can simply do: *)
by move=> ->.
Qed.

(* We use apply: to proceed by backward reasonning on the whole goal *)

Lemma impl_1 (A B C : Prop) : (A -> B -> C) -> (B -> A -> C).
Proof.
move=> implH pB pA.
apply: implH.
  by [].
by [].
Abort.

Lemma impl_1 (A B C : Prop) : (A -> B -> C) -> (B -> A -> C).
Proof.
move=> implH pB pA.
by apply: implH.
Abort.

Lemma impl_1 (A B C : Prop) : (A -> B -> C) -> (B -> A -> C).
Proof.
move=> implH pB pA.
by move: (implH pA pB).
Abort.

Lemma impl_1 (A B C : Prop) : (A -> B -> C) -> (B -> A -> C).
Proof.
move=> implH pB pA.
apply: (implH pA pB).
Abort.


Lemma impl_1 (A B C : Prop) : (A -> B -> C) -> (B -> A -> C).
Proof.
move=> implH pB pA.
by apply: implH.
Qed.

Lemma impl_2 (A B C : Prop) : (A -> B) -> (A -> C) -> (A -> B /\ C).
Proof.
move=> implAB implAC pA.
split.
  by apply: (implAB pA).
by apply: (implAC pA).
Qed.

Lemma impl_3 (A B C : Prop) : (A /\ B) -> (A -> B -> C) -> C.
Proof.
move=> AnB implH.
apply: implH.
  by elim: AnB. 
by elim: AnB.
Qed.

Lemma exmaple_prop n m :
  n = 2 /\ m = 3 -> n + m = 5.
Proof.
by move=> [neq2 meq3]; rewrite neq2 meq3.
Undo.
by move=> [-> ->].
Qed.

(*
   Propositions provide a more expressive logic
   Booleans provide computation & Excluded Middle

   To get the best of each world, we link them using the reflect predicate
 *)


Search "andP".

Lemma example n m :
  (n == 2) && (m == 3) -> (n + m = 5).
Proof.
Fail move=> [-> ->].
move=> /andP.
Fail move=> [-> ->].
Print eqP.
move=> [/eqP neq2 /eqP meq3].
Undo.
by move=> [/eqP -> /eqP ->].
Qed.

Lemma leq_total m n : (m <= n) || (m >= n).
Proof.
Print implyNb.
Print ltnNge.
rewrite -implyNb -ltnNge.
apply/implyP.
Print ltnW.
by apply: ltnW.
Qed.

Lemma andP_rev (b1 b2 : bool) : reflect (b1 /\ b2) (b2 && b1).
Proof.
Print iffP.
Print idP.
apply: (iffP idP).
  by move=> /andP [-> ->].
by move=> /andP; rewrite andbC.
Abort.

Lemma andP_rev (b1 b2 : bool) : reflect (b1 /\ b2) (b2 && b1).
Proof.
by apply: (iffP idP) => /andP => [[-> ->] //|]; rewrite andbC.
Qed.

Lemma maxn_leqP m n : reflect (maxn m n = m) (m >= n).
Proof.
by rewrite /leq -(eqn_add2l m) addn0 maxnE; apply/eqP.
Qed.

(* We use case: ifP to reason about if then else constructions  *)

Definition max3 n0 n1 n2 := if n0 < n1 then maxn n1 n2 else maxn n0 n2.

Lemma L2 n0 n1 n2 : max3 n0 n1 n2 = maxn (maxn n0 n1) n2.
Proof.
rewrite /max3.
case: ifP.
  by move=> /ltnW /maxn_idPr ->.
by move=> /negbT; rewrite leqNgt negbK => /ltnSE /maxn_leqP ->.
Qed.
