(* SESSION #1: FUNCTIONS; BASIC TYPES & CONTAINERS *)


(*
   This is not standard Coq!
   We will be using the mathematical components library:

   - formalization techniques
   - proof language
   - large consistent library.

   Small Scale Reflection (SSReflect) is a methodology for developing proofs
   in Coq that aims to enhance automation of computational steps of the theorem
   prover.

   Whenever working with a "computable" mathematical object, we want to
   represent it as a computable function i.e. a program; not as an
   logical relation.
   The computational content can be expressed within Coq and Coq knows how to
   compute. Thus, we can use computation as a stable form of proof automation.
*)

From mathcomp Require Import all_ssreflect.

(* nat is a predefined type for the natural numbers in the initial library *)

Print nat.

Check nat.

(*
  In this session we're going to practice how to define functions. A way to build
  functions is by using λ-expressions by means of the syntax:
           fun x => e
  The idea behind this syntax is that we map the variable x to the expression e. *)

Definition nat_id := fun (n : nat) => n.

Check nat_id.

Print nat_id.

(*
  f : A -> B -> C should be read as f : A -> (B -> C)
  Thus, for example f : nat -> nat -> nat is Coq syntax for the common
  mathematical expression:

  f : N x N -> N

  i.e.

  f : N ^ 2 -> N
*)

Compute nat_id 120.

Definition nat_id2 (n : nat) := n.

Print nat_id2.

Definition nat_id3 (n : nat) : nat := n.

Definition nat_id4 : nat -> nat := fun n => n.

(* High-order functions *)

Definition HOfun (f : nat -> nat) (n : nat) := f n.

(* Exercise: Check? *)

Compute HOfun succn 10.


(* Observe that the type of a function changes as we feed the function. Let us
   consider the following example where we have a function f : A -> B -> C -> D,
   with a : A, b : B and c : C. Then:

   1. f: A -> B -> C-> D;
   2. f a: B -> C -> D;
   3. f a b : C -> D;
   4. f a b c : D.

   This is consistent since the term itself is changing. In case 1, the term is
   simply f while in case 2, the term is f a, which is a different object,
   etc. *)

Check HOfun succn.

(* nat - arithmetic (and notations) *)

Check succn. (* n.+1 *)

Check predn. (* n.-1 *)

Check addn. (* + *)

Check muln. (* * *)

Check subn. (* - *)

Check divn. (* %/ *)

Check expn. (* ^ *)

(* Omitting type-bindings *)

Definition arith_op1 n m := (n * 2) + m.


(* booleans are also pre-defined *)

Print bool.

(* bool operations *)

Check andb. (* && *)

Check orb. (* || *)

Check negb. (* ~~ *)

Check implb. (* ==> *)

Check eqb. (* == *)

(* bool - nat operations *)

Check leq.  (* <=
  From this primitive operation we define <, >, <= with their natural meaning *)

Check eqn. (* == *)

(* Observe that the notation for eqn and eqb is the same. In general, we will
   use the same notation whenever the type is equiped with a decidable equality
   relation. *)


(* if ... then ... else ... *)

(* We can use it in the standard way, were we have:

     if (b : bool) then (consequent) else (alternative),

   And so, when b is evaluated to true, the function will compute the consequent
   and the alternative, when b is evaluated to false.

 *)

Definition twoVthree (b : bool) := if b then 2 else 3.

Compute twoVthree true.

Compute twoVthree false.

Compute twoVthree (0 < 1).

Compute twoVthree (1 == 0).

Definition max n m := if n < m then m else n.

Compute max 12 1234.

(* We have also an extended use of the if ... then ... else statement for
   pattern matching
*)

Definition pred2 n := if n is k.+1.+1 then k else 0.

Compute pred2 2.

Compute pred2 5.

Compute pred2 1.

Compute pred2 0.

(* This use is syntax sugar for match ... with ... end *)

Definition pred2_match n :=
  match n with
  | k.+1.+1 => k
  | k.+1 => 0
  | 0 => 0
  end.

(* Or simply: *)

Definition pred2_match2 n :=
  match n with
  | k.+1.+1 => k
  | _  => 0
  end.

(*
  Containers:

  Containers let us aggregate data in a single object. Containers are usually
  polymorphic, that is: the same container can be used to hold terms of many
  types.
*)

(* Sequences (Mathcomp) / Lists (vanilla coq) *)

Print seq.

Print list.

Check [:: 0; 1].

Check [:: true; false; true].

Check cons 0 (cons 1 nil).

Check cons true (cons false (cons true nil)).

Check [::].

Check nil.

(* Check [:: 0; 1; false]. *)

Definition seq_nat_example := [:: 2; 3].

Definition seq_bool_example := [:: true; false].

Compute 0 :: seq_nat_example.

Compute (0 <= 1) :: (1 < 0) :: seq_bool_example.

(* seq operations *)

Check cat. (* ++ *)

Compute [:: 0] ++ seq_nat_example.

(* Compute 0 :: seq_nat_example == [:: 0] ++ seq_nat_example. *)

(* Compute (0 :: seq_nat_example == [:: 0] ++ seq_nat_example) :: seq_bool_example. *)

Check rev.

Check size.
