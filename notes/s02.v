(* SESSION #2: INDUCTIVE TYPES & RECURSIVE FUNCTIONS *)

From mathcomp Require Import all_ssreflect.

(* We are going to focus now on how can we define our own inductive types in
   Coq. We declare them by using the Inductive command. *)

Inductive threecolors : Set :=
| green : threecolors
| yellow : threecolors
| red : threecolors.

(* or simply *)

Inductive threecolors2 :=
| green2
| yellow2
| red2.

(* Every time we declare a new type like:
      Inductive T := t0 | f1 (t : T) | f2 (t : T).
   there are two implicit declarations:
   • The constructors f1 and f2 are injective;
   • The constructors are disjoint. That is, t0 is different from f1 t or f2 t,
     for any t : T. Moreover, f1 t is different from f2 t, for any t : T. *)


(* Constructors can be functions with a different domain type *)

Inductive fourcolors :=
| c : threecolors -> fourcolors
| blue.

(* Mutually inductive definitions i.e. definitions of multiple types that
   depend on each other. *)

Inductive week :=
| mon
| tues
| wed
| thu
| fri
| w : weekend -> week
with weekend := sat | sun.

(* We can also use Inductive to define n-ary predicates. *)

(* Prop is the type of logical propositions,  built according to the
   following syntax:
   1. Tautology: True : Prop
   2. Falsum: False : Prop
   3. Identity relation: t = u : Prop
   4. Non-Identity: t <> u : Prop
   5. Negation: ~ P : Prop
   6. Disjunction: P \/ Q : Prop
   7. Conjuction: P /\ Q : Prop
   8. Implication: P -> Q : Prop
   9. Bi-implication: P <-> Q : Prop
   10. Existential quantificaction: exists (x : T), P : Prop
   11. Universal quantification: forall (x : T), P : Prop
  where t and u are terms, and P Q : Prop.

 Moreover, recall that Set : Type(1)
                       Prop : Type(1)
                       Type(i) : Type(i+1) *)

(* We can define even, a unary predicate over the natural numbers, declaring
   that a particular number n is even: *)

Inductive even : nat -> Prop :=
| even0 : even O
| evenS : forall n, even n -> even n.+2.

(* Exercise: Remove the above definition and give mutual inductive definitions
   for even and odd *)

(* -------------------------------------------------------------------------- *)

(* Recursive functions *)

(* To define recursive functions we use the Fixpoint command. To be accepted, a
   Fixpoint definition has to satisfy a syntactical constraint on a special
   argument called the (structurally) decreasing argument, needed to ensure
   that the definition always terminates. *)

Fixpoint add_if n m := if m is k.+1 then (add_if n k).+1 else n.

(* This is not structurally decreasing:

   Fixpoint add_if n m := if m is k.+1 then (add_if n m.-1).+1 else n. *)

(* There is yet another way of defining recursive functions. In SSReflect we have the
function iter m f x that iterates the function f, m times, over the term x. *)

Fixpoint addn_it n m := iter m succn n.

(* let-in & let fix-in *)

(* let ident := term1 in term2 represents the local binding of the variable
   ident to the value term1 in term2. *)

Definition arith_op1 n m := (n * 2) + m.

Definition arith_op2 n m := let k := (n * 2) in k + m.

Lemma arith_op12_eq : arith_op1 =1 arith_op2.
Proof. by []. Qed.

(* We can also declare a local recursive construction making use of the let-in
   syntax together with the fix command *)

Definition add_10 n :=
    let fix add_aux n m := if m is k.+1 then (add_aux n k).+1 else n in
  add_aux n 10.


