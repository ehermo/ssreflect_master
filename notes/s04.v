(* SESSION #4: PROOFS BY INDUCTION && REWRITE *)

From mathcomp Require Import all_ssreflect.

(* In s03.v we had: *)
Lemma succn_addn n m : n.+1 + m = (n + m).+1.
Proof.
by [].
Qed.

Lemma addn0 m : m + 0 = m.
Proof.
elim: m.
  by [].
move=> n IH.
rewrite succn_addn.
rewrite IH.
by [].
Qed.

Lemma addn0_efficient m : m + 0 = m.
Proof.
elim: m => [|m IH].
  by [].
by rewrite succn_addn IH.
Qed.

Lemma muln0 m : m * 0 = 0.
Proof.
elim: m => [|m IHm].
  by [].
(* Search succn muln in ssrnat. *)
by rewrite mulSnr IHm.
Qed.

Lemma muln_eq0_or m n :
  (m * n == 0) = (m == 0) || (n == 0).
Proof.
case: m => [|m].
  by [].
case: n => [|n].
(* Search 0 muln in ssrnat *)
(* Print right_zero *)
  by rewrite muln0.
by [].
Qed.

Lemma leq_mul2l m n1 n2 :
  (m * n1 <= m * n2) = (m == 0) || (n1 <= n2).
Proof.
(* Print leq *)
rewrite /leq.
(* Search muln subn in ssrnat. *)
(* Print right_distributive. *)
rewrite -mulnBr.
rewrite muln_eq0_or.
by [].
Qed.

Lemma leq_mul2l_efficient m n1 n2 :
  (m * n1 <= m * n2) = (m == 0) || (n1 <= n2).
Proof.
by rewrite /leq -mulnBr muln_eq0_or.
Qed.

Lemma moveandrewrite b : b = true -> ~~ b = false.
Proof.
move=> bistrue.
by rewrite bistrue.
(*by move=> ->. *)
Qed.
