# Master_SSReflect

Here you can find the notes, exercises and solutions for the Coq part of the course.

## Getting started

To use Coq in your browser, go [here](https://coq.vercel.app/scratchpad.html).

## Schedule

Computer room IG:

- 4/12 (Monday) 15:00 - 17:00;

- 11/12 (Monday) 15:00 - 17:00;

- 18/12 (Monday) 15:00 - 17:00;

- 20/12 (Wednesday) 12:00 - 14:00;

- 9/1 (Tuesday) 11:00 - 12:00;

- 10/1 (Wednesday) 12:00 - 14:00;

- 16/1 (Tuesday) 11:00 - 12:00;

- 17/1 (Wednesday) 12:00 - 14:00;

- 22/1 (Tuesday) 11:00 - 12:00.

## Contact
You can reach me at ```rev [:: ".com"; "gmail"; "@"; "reyes"; "ehermo."]```.

## Project status
The content of this repository is only for educational purposes. Please do not distribute.
