(* EXERCISES #1: FUNCTIONS; BASIC TYPES & CONTAINERS *)

From mathcomp Require Import all_ssreflect.


(*
  1. Define arbitrary functions of type:

       a. nat -> nat;
       b. bool -> bool -> bool -> bool;
       c. (nat -> nat -> nat) -> nat -> nat -> nat.

*)


(*
  2. Define the logical operation known as the Sheffer stroke and use the
     Compute command to test it:

       p q | ShS
       1 1 |  0
       1 0 |  1
       0 1 |  1
       0 0 |  1
*)

Definition ShS :=

Compute ShS

(*
  3. Define the function check_add satisfying the following specification:

       a. check_add: nat -> nat -> nat -> bool;

       b. check_add(n, m, r) = true,  if n + m = r
                               false, otherwise
*)

Definition check_add :=

(*
  4. Define the function headl2 satisfying the following specification:

       a. headl2 : list nat -> list nat

       b. headl2(l) = [:: n0],   if l := [:: n0; n1]
                      [::],      otherwise

*)

Definition headl2 :=
