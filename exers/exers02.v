From mathcomp Require Import all_ssreflect.

(* Given the definition: *)

Inductive T := con (n : nat).

(* Define a function con1 : T -> nat such that con1 (con n) = n. *)

Definition con1 t :=

(* Define the following objects:
   1. The set month containing the months of the year: January, February, etc;
   2. The function next_month : month -> month that computes the succeeding
      month i.e. next_month January = February, etc. *)


(* Define the regular mathematical factorial function. *)

(* Define the following predicates:
   1. A binary predicate fibo such that fibo n m specifies that m is the n-th
      Fibonacci number;
   2. A ternary predicate coll such that coll n m p specifies that p is the m-th
      number in the Collatz sequence starting with n. *)


(* Implement the Ackermann function. *)

(* Using ithe existing iter and map functions to define a function that maps the
   natural number n to the list containing the n first odd numbers. *)

Print map.

Definition odds n :=

(* Define a type formalizing the set of propositional modal formulas built from
   the following syntax:
    p_i for i \in N | ~ A | A /\ B | [] A  *)

Inductive mod_forms :=
