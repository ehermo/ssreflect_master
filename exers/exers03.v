From mathcomp Require Import all_ssreflect.


(* Prove the following lemmas: *)

Lemma exer0 p : true && p = p.

Lemma exer1 p q : p || q = q || p.

Lemma exer2 p q : p ==> q = ~~ p || q.

Lemma exer3 b : ~~ ~~ ~~ b = ~~ b.

Lemma exer4 n m : False -> n + m = n - m.

Lemma exer5 n : (0 == 0) = (0 == 1) -> n < n - 1.

Lemma exer6 b0 b1 b2 b3 : ~~ (b0 && ~~ ~~ ~~ b0) || ((b1 && ~~ b2) || ~~ b3).

(* In the mathcomp library we have the operation maxn : nat -> nat -> nat.

   Use maxn to define the function max3 : nat -> nat -> nat -> nat such that
   max3 n0 n1 n2 returns the maximum of n0, n1 and n2.

   State and prove the following properties:
   1. ∀n ∈ N, max3(n, n, n) = n;
   2. ∀n0, n1, n2 ∈ N, max3(n0, n1, n2) = maxn(maxn(n0, n1), n2);
   3. ∀n0, n1, n2 ∈ N, max3(n0, n1, n2) = max3(n1, n0, n2);  *)

Definition max3

Lemma L1

Lemma L2

Lemma L3

(* Copy-paste the solutions to exercise 2 in exers02.v. Using the injective
   predicate, state and prove that next_month is injective *)

Print injective.

Lemma next_month_inj
